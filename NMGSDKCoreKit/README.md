# NMGSDKCoreKit

[![CI Status](http://img.shields.io/travis/SIUK LEE/NMGSDKCoreKit.svg?style=flat)](https://travis-ci.org/SIUK LEE/NMGSDKCoreKit)
[![Version](https://img.shields.io/cocoapods/v/NMGSDKCoreKit.svg?style=flat)](http://cocoapods.org/pods/NMGSDKCoreKit)
[![License](https://img.shields.io/cocoapods/l/NMGSDKCoreKit.svg?style=flat)](http://cocoapods.org/pods/NMGSDKCoreKit)
[![Platform](https://img.shields.io/cocoapods/p/NMGSDKCoreKit.svg?style=flat)](http://cocoapods.org/pods/NMGSDKCoreKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NMGSDKCoreKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NMGSDKCoreKit'
```

## Author

SIUK LEE, lsu8290@netmarble.com

## License

NMGSDKCoreKit is available under the MIT license. See the LICENSE file for more info.
