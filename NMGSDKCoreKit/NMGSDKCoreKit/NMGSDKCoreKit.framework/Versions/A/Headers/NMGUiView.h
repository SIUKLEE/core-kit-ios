//
//  NMGUiView.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 21..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGUiViewHandler.h>

/**
 * @brief This NMGUiView class provides API functions to show all NetmarbleS views.
 */
@interface NMGUiView : NSObject

+ (void)showWithLocation:(int)location eventHandler:(void(^)(NMGUiViewState uiViewState))eventHandler;

@end
