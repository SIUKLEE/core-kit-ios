//
//  NMGUtil.h
//  NMCommon
//
//  Copyright (c) 2014 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief This NMGUtil class provides the utility API for the conveniene of developers.
 */
@interface NMGUtil : NSObject

/**
 * @brief If you want to a device timezone, this method call.
 *
 * @return timezone ex)+09:00
 */
+ (NSString *)timeZone;

/**
 * @brief If you want to a IDFA, this method call.
 *
 * @return IDFA
 */
+ (NSString *)identifierForAdvertising;

/**
 * @brief If you want to a device unique ID, this method call.
 *
 * @return NMDeviceKey
 */
+ (NSString *)NMDeviceKey;

@end
