//
//  NMGTCPSession.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 12. 9..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BasePacket;
@class SessionInfo;

@interface NMGTCPSession : NSObject

+ (void)sendMessageWithBasePacket:(BasePacket *)basePacket;

/*!
 * @brief 연결 상태에 대한 응답을 받을 수 있습니다.
 *
 * @param delegate delegate는 NMGTCPSessionDelegate를 conform 해야합니다.
 *
 * @return BOOL 정상적으로 delegate가 등록되면은 YES,
 * delegate가 nil이거나 NMGTCPSessionDelegate를 conform하지 않았으면 NO
 * @see NMGTCPSessionDelegate
 */
+ (BOOL)setDelegate:(id)delegate;

/*!
 * @brief 연결 상태에 대한 응답 해제합니다.
 *
 * @return BOOL 정상적으로 delegate가 해제되면은 YES,
 * delegate가 nil이면은 NO
 */
+ (BOOL)removeDelegate;

/*!
 * @brief Session 서버에 연결을 합니다.
 *
 * 연결 동작 순서
 *
 * 1. 세션 서버 매니저에 HOST 와 PORT 정보를 획득 합니다.
 *
 * 2. HOST 와 PORT 정보로 소켓 연결을 시도 합니다.
 *
 * 3. 연결이 성공하면, signIn request packet 을 전송합니다
 *
 * 4. signIn response packet 정상적으로 받으면 didConnectWithSessionID: 로 콜백을 전달하고, 지속적으로 연결을 유지 합니다.
 *
 * 연결이 정상적으로 수행된 경우 didConnectWithSessionID: 로 응답이 옵니다.
 *
 * Note : worldID 나 characterID 정보가 바뀔 경우 connectWithUserData: 를 다시 호출 해야 합니다.
 *
 * 호출 하지 않을 경우 게임의 정보와 넷마블 서버의 정보가 일치 하지 않아,
 * 동작이 기대값과 다른 경우가 발생 할 수 있습니다.
 *
 * characterID : 제한없는 문자열. 최대 50자 
 *
 * worldID : 영문 소문자, 숫자 최대 10자
 *
 * @code NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:
                                                @"world01", @"worldID",
                                                @"characterID01", @"characterID",
                                                nil];
 
 NSNumber *result = [NMGTCPSession connectWithUserData:userData];
 NSLog(@"[NMGTCPSession connectWithUserData:] : %d", [result intValue]);
 * @endcode
 *
 * @param userData worldID, characterID<br>
 *
 * @return BOOL 정상적으로 delegate가 등록되면은 YES,
 * delegate가 nil이거나 NMGTCPSessionDelegate를 conform하지 않았으면 NO
 */
+ (NSNumber *)connectWithUserData:(NSDictionary *)userData;

/*!
 * @brief Session 연결을 해제 합니다.
 *
 * @return BOOL Session 연결이 정상적으로 해제 되었습니다.
 * Session이 이미 해제된 상태라면은 false
 */
+ (BOOL)disconnect;

/*!
 * @brief Session 연결이 되어있는지 조회합니다.
 *
 * @return BOOL Session 연결이 되어있으면 YES, 연결되지 않은 상태면은 NO
 */
+ (BOOL)isConnected;

+ (SessionInfo *)getSessionInfo;

@end

@protocol NMGTCPSessionDelegate

@optional
- (void)didConnectWithSessionID:(NSString *)sessionID;
- (void)didDisconnect;
- (void)didReconnectWithCause:(NSNumber *)cause;

@end
