//
//  NMGPush.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 21..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGWorldAllowPushNotification.h>

@class NMGResult;

/**
 * @brief This NMGPush class provides push API functions.
 */
@interface NMGPush : NSObject

/**
 * @brief Sends a push message to other users.
 *
 * @param message Push message.
 * @param playerIDArray The array of player ID. The array of NSString instance.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
+ (void)sendPushNotificationWithMessage:(NSString *)message
                          playerIDArray:(NSArray *)playerIDArray
                      completionHandler:(void(^)(NMGResult *result))completionHandler;

/**
 * @brief This is used to set the on/off value for use of push notification.<br>
 * The default value is set to ‘true' to use push function.
 *
 * @param use Use of push setting either 'YES' or 'NO'.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
+ (void)setUsePushNotificationWithUse:(BOOL)use completionHandler:(void(^)(NMGResult *result))completionHandler __deprecated;

/**
 * @brief Gets the current on/off value for use of push notification from the NetmarbleS server.
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * - use Use of push notification.
 * @see NMGResult
 */
+ (void)getUsePushNotificationWithCompletionHandler:(void(^)(NMGResult *result, BOOL use))completionHandler __deprecated;

/**
 * @brief Sets a local notification message at scheduled time.
 *
 * @param sec Second.
 * @param message Message.
 * @param soundName Sound file name. nil if don't use sound.
 * @param userInfo User information to be delivered with push message.
 */
+ (int)setLocalNotificationWithSec:(int)sec
                           message:(NSString *)message
                         soundName:(NSString *)soundName
                          userInfo:(NSDictionary *)userInfo;


/**
 * @brief Cancels the local notification that already set to be delivered.
 *
 * @param localPushID Local push ID to be canceled.
 */
+ (BOOL)cancelLocalNotificationWithLocalPushID:(int)localPushID;

/**
 * @brief This is used to set the on/off value for use of notice push, game push, nightNotice push notification.<br>
 * The default value is set to ‘true' to use push function.
 *
 * @param notice Use of notice push setting either 'None' or 'On' or 'Off'.
 * @param game Use of game push setting either 'None' or 'On' or 'Off'.
 * @param nightNotice Use of nightNotice push setting either 'None' or 'On' or 'Off'.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 * @see NMGAllowPushNotification
 */
+ (void)setAllowPushNotificationWithNoticePush:(NMGAllowPushNotification)notice
                                      gamePush:(NMGAllowPushNotification)game
                               nightNoticePush:(NMGAllowPushNotification)nightNotice
                             completionHandler:(void(^)(NMGResult *result))completionHandler;


/**
 * @brief Gets the current on/off value for use of notice push, game push, nightNotice push notification from the NetmarbleS server.
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * - notice Use of notice push notification.
 * - game Use of game push notification.
 * - nightNotice Use of nightNotice push notification.
 * @see NMGResult
 * @see NMGAllowPushNotification
 */
+ (void)getAllowPushNotificationWithCompletionHandler:(void(^)(NMGResult *result,
                                                               NMGAllowPushNotification notice,
                                                               NMGAllowPushNotification game,
                                                               NMGAllowPushNotification nightNotice))completionHandler;

/**
 * @brief 월드별 공지, 게임, 야간공지의 푸시 ON/OFF 설정을 할 수 있습니다.
 *
 * @param worldAllowPushNotificationArray WorldAllowPushNotification List
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 * @see NMGWorldAllowPushNotification
 */
+ (void)setWorldsAllowPushNotification:(NSArray *)worldAllowPushNotificationArray
                     completionHandler:(void (^)(NMGResult *result))completionHandler;

/**
 * @brief 월드별 공지, 게임, 야간공지의 푸시 ON/OFF 여부를 알 수 있습니다.
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * - worldAllowPushNotificationArray worldAllowPushNotificationArray
 * @see NMGResult
 * @see NMGAllowPushNotification
 */
+ (void)getWorldsAllowPushNotificationWithCompletionHandler:(void (^)(NMGResult *result, NSArray *worldAllowPushNotificationArray))completionHandler;

@end
