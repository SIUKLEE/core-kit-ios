//
//  NMGTransferProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 12. 12..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

@class NMGTransferMessage;

@protocol NMGTransferProtocol

@required
+ (instancetype)create;

@optional

- (void)didReceiveTransferMessage:(NMGTransferMessage *)transferMessage;

@end
