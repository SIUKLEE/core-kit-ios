//
//  NMGResult.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 11. 25..
//  Copyright (c) 2015년 Netmarble Games. All rights reserved.
//

/**
 * @brief The result code.
 */
#define SUCCESS                                             (0)				/**< Success. */

#define UNKNOWN                                             (0x00010001)	/**< Unknown error. */
#define SERVICE                                             (0x00010002)	/**< Service error. */
#define NETWORK_UNAVAILABLE                                 (0x00010003)	/**< Network unavailable. */
#define TIMEOUT                                             (0x00010004)	/**< Time out. */

#define USER_CANCELED                                       (0x00020001)	/**< Canceled by user. */
#define INVALID_TOKEN                                       (0x00020002)	/**< Invalid token. */

#define NOT_AUTHENTICATED                                   (0x00030001)	/**< Not authenticated. */
#define INVALID_PARAM                                       (0x00030002)	/**< Invalid parameter. */
#define NOT_SUPPORTED                                       (0x00030003)	/**< Not supported. */
#define PERMISSION                                          (0x00030004)	/**< No permission. */
#define JSON_PARSING_FAIL                                   (0x00030005)	/**< Json parsing error. */
#define IN_PROGRESS                                         (0x00030006)    /**< In progress. */

#define CONNECT_CHANNEL_OPTION_CHANNEL_CONNECTION_MODIFIED  (0x00050001)    /**< Channel connection modified. */
#define CONNECT_CHANNEL_OPTION_USED_CHANNELID               (0x00050002)    /**< Used channelID. */
#define CONNECT_CHANNEL_OPTION_NEW_CHANNELID                (0x00050003)    /**< New channelID. */

#define OTP_IS_NOT_VALID                                    (0x00060001)    /**< OTP is not valid. */
#define OTP_WAS_EXPIRED                                     (0x00060002)    /**< OTP was expired. */
#define OTP_INPUT_RESTRICTED                                (0x00060003)    /**< OTP input restricted. */

#define DISABLE_AUTHENTICATED                               (0x00070001)    /**< Disable authenticated. */

/*
 * @since 4.1.4
 * @brief 새로 정의된 에러코드
 */
#define DETAIL_PLAYERID_MOVED                               (-101104)

#define DETAIL_USED_CHANNELID                               (-102101)
#define DETAIL_NEW_CHANNELID                                (-102102)

#define DETAIL_OTP_WAS_EXPIREXD                             (-105100)
#define DETAIL_OTP_INPUT_RESTRICTED                         (-105101)
#define DETAIL_OTP_INVALID                                  (-105102)

/**
 * @brief The result domain.
 */
#define NETMARBLES_DOMAIN        (@"NETMARBLES_DOMAIN")         /**< NetmarbleS Domain. */
#define FACEBOOK_DOMAIN          (@"FACEBOOK_DOMAIN")           /**< Facebook Domain. */
#define APPLE_GAMECENTER_DOMAIN  (@"APPLE_GAMECENTER_DOMAIN")   /**< Apple GameCenter Domain. */
#define KAKAO_DOMAIN             (@"KAKAO_DOMAIN")              /**< Kakao Domain. */

#import <Foundation/Foundation.h>

/**
 * @brief This NMGResult class is used to manage the result information generated from Netmarble SDK.
 */
@interface NMGResult : NSObject

/**
 * @brief Result domain.(read only)
 */
@property (nonatomic, retain, readonly) NSString *domain;

/**
 * @brief Result code.(read only)
 */
@property (nonatomic, assign, readonly) int code;

/**
 * @brief Result message.(read only)
 */
@property (nonatomic, retain, readonly) NSString *message;

/**
 * @brief Result detail code.(read only)
 */
@property (nonatomic, assign, readonly) int detailCode;


- (id)initWithDomain:(NSString *)domain code:(int)code message:(NSString *)errorMessage;
- (id)initWithDomain:(NSString *)domain code:(int)code message:(NSString *)errorMessage detailCode:(int)detailCode;

/**
 * @brief Returns a boolean value that indicates whether the result is success.
 *
 * @return Ruturns 'YES' if the result is success. Otherwise, it returns 'NO'.
 *
 * @code
 * if ([error isSuccess] == YES)
 *      NSLog(@"Success");
 * @endcode
 */
- (BOOL)isSuccess;

@end
