//
//  NMGChannelConnectOption.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 30..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGSession.h>

typedef enum
{
    NMGChannelConnectOptionTypeCancel = 0x00,
    NMGChannelConnectOptionTypeUpdateChannelConnection,
    NMGChannelConnectOptionTypeLoadChannelConnection,
    NMGChannelConnectOptionTypeCreateChannelConnection,
} NMGChannelConnectOptionType;

/**
 * @brief This NMGChannelConnectOption provide one channel connect option info.
 */
@interface NMGChannelConnectOption : NSObject

/**
 * @brief Gets the player ID.
 *
 * @return Player ID.
 */
@property (nonatomic, retain, readonly) NSString *playerID;

/**
 * @brief Gets the channel ID.
 *
 * @return Channel ID.
 */
@property (nonatomic, retain, readonly) NSString *channelID;

/**
 * @brief Gets the region.
 *
 * @return Region.
 */
@property (nonatomic, retain, readonly) NSString *region;

/**
 * @brief Gets the channel.
 *
 * @return Channel.
 */
@property (nonatomic, assign, readonly) int channel;

/**
 * @brief Gets the channel option type.
 *
 * @return ChannelConnectOptionType.
 * @see NMGChannelConnectOptionType
 */
@property (nonatomic, assign, readonly) NMGChannelConnectOptionType type;

@end
