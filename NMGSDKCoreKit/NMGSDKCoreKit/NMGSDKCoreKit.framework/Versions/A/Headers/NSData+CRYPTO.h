//
//  NSData+CRYPTO.h
//  NMCommon
//
//  Created by SIUK LEE on 2014. 7. 23..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NMG_CRYPTO)

+ (NSData*)dataFromHexString:(NSString*)hexString;

- (NSString*)toHexString;
- (NSData *)AES256EncryptWithKey:(NSString *)key initializationVector:(NSString*)vector;
- (NSData *)AES256DecryptWithKey:(NSString *)key initializationVector:(NSString*)vector;
- (NSData *)RC4EncryptWithKey:(NSString *)key;
- (NSData *)RC4DecryptWithKey:(NSString *)key;

@end
