//
//  NMGAppEvents.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 5. 3..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMGAppEvents : NSObject

+ (void)setRewardHandler:(void(^)())rewardHandler;

+ (void)setDeepLinkHandler:(void(^)(NSURL *url))handler;

@end
