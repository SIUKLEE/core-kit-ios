//
//  NMGTCPSessionProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 12. 9..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

@class NMGResult;
@class BasePacket;

@protocol NMGTCPSessionProtocol <NMGSessionStateProtocol>

@required
+ (instancetype)create;

@optional
- (NSString *)serviceName;

- (void)TCPSessionDidSignResult:(NMGResult *)result;
- (void)TCPSessionDidDisconnected;
- (void)TCPSessionDidReceiveBasePacket:(BasePacket *)basePacket;

@end
