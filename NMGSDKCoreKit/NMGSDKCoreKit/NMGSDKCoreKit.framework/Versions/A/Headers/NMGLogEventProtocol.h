//
//  NMGLogEventProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 5. 12..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NMGLogEventProtocol <NSObject>

+ (id)create;
- (void)onLogWithLogID:(NSNumber *)logID detailID:(NSNumber *)detailID logDesc:(NSDictionary *)logDesc;

@end