//
//  NMGRequestProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 4. 20..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <NMGSDKCoreKit/NMGSessionStateProtocol.h>

@protocol NMGRequestProtocol <NSObject, NMGSessionStateProtocol>

+ (id)create;

- (void)initialize;

@end
