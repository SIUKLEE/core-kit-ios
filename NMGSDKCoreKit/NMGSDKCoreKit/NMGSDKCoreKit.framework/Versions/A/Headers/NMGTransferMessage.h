//
//  NMGTransferMessage.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 12. 15..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NMG_TRANSFER_MESSAGE_TYPE_REQUEST           0
#define NMG_TRANSFER_MESSAGE_TYPE_RESPONSE          1

#define NMG_TRANSFER_MESSAGE_RESULT_SUCCESS         0
#define NMG_TRANSFER_MESSAGE_RESULT_FAIL            1
#define NMG_TRANSFER_MESSAGE_RESULT_NOT_FOUND       2
#define NMG_TRANSFER_MESSAGE_RESULT_NOT_DEFINED     3

@class NMGResult;

@interface NMGTransferMessage : NSObject

@property (nonatomic, retain) NSString *sender;
@property (nonatomic, retain) NSString *receiver;

@property (nonatomic, retain) NSString *action;

@property (nonatomic, retain) NSDictionary *requestParams;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int requestCode;

@property (nonatomic, assign) int resultCode;
@property (nonatomic, retain) NSDictionary *resultData;

@end
