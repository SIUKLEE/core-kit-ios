//
//  NSData+GZIP.h
//  NMCommon
//
//  Created by SIUK LEE on 2014. 7. 23..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NMG_GZIP)

- (NSData *)gzippedDataWithCompressionLevel:(float)level;

- (NSData *)gzippedData;

- (NSData *)gunzippedData;

@end
