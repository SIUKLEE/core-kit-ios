//
//  NSArray+NMG.h
//  NetmarbleS
//
//  Created by SIUK LEE on 2014. 12. 15..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(NMG)

- (NSData *) getJsonData;
- (NSString *) getJsonString;

@end


