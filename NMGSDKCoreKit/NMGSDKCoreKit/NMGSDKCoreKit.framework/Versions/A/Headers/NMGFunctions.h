//
//  NMGFunctions.h
//  NetmarbleS v.2.0.0
//
//  Created by Mobile Platform Development Team on 2013. 5. 1.
//  Copyright (c) 2013 CJ E&M. All rights reserved.
//

#import <UIKit/UIKit.h>

// #define NMImageData(key) [[NMManager sharedInstance] getImageData:(key)]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 \
alpha:1.0]

#define UIColorFromARGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0x00FF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x0000FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x000000FF))/255.0 \
alpha:((float)((rgbValue & 0xFF000000) >> 24))/255.0]

inline static UIViewController *topMostController()
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    return topController;
}

inline static NSString *ConvertNilToEmptyNSString(NSString *string)
{
    return (nil == string) ? @"" : string;
}

inline static BOOL IsEmptyNSString(NSString *string)
{
    if ((nil == string) || (0 == string.length))
    {
        return YES;
    }
    return NO;
}

inline static NSString* removeScheme(NSString *string)
{
    NSRange range = [string rangeOfString:@"://"];
    NSString *replaceString = [string substringFromIndex:range.location + 3];
    
    if (nil != replaceString || 0 < replaceString.length)
    {
        return replaceString;
    }
    return string;
}

inline static NSData* DataWithBase64EncodedString(NSString *string)
{
    static const char encodingTable[] ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    if(nil == string){
        return [NSData data];
    }
    if(0 == [string length]){
        return [NSData data];
    }
    
    static char *decodingTable = NULL;
    if(decodingTable == NULL){
        decodingTable = (char *)malloc(256);
        if(decodingTable == NULL){
            return nil;
        }
        memset(decodingTable, CHAR_MAX, 256);
        NSUInteger i;
        for(i = 0; i < 64; i++){
            decodingTable[(short)encodingTable[i]] = i;
        }
    }
    
    const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
    if(NULL == characters){ //  Not an ASCII string!
        return nil;
    }
    char *bytes = (char *)malloc((([string length] + 3) / 4) * 3);
    if(bytes == NULL){
        return nil;
    }
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while(YES){
        char buffer[4];
        short bufferLength;
        for(bufferLength = 0; bufferLength < 4; i++){
            if(characters[i] == '\0'){
                break;
            }
            if(isspace(characters[i]) || characters[i] == '='){
                continue;
            }
            buffer[bufferLength] = decodingTable[(short)characters[i]];
            if(buffer[bufferLength++] == CHAR_MAX){ //  Illegal character!
                free(bytes);
                return nil;
            }
        }
        
        if(0 == bufferLength){
            break;
        }
        if(1 == bufferLength){ // at least two characters are needed to produce one byte!
            free(bytes);
            return nil;
        }
        
        // decode the characters in the buffer to bytes.
        bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
        if(bufferLength > 2){
            bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
        }
        if(bufferLength > 3){
            bytes[length++] = (buffer[2] << 6) | buffer[3];
        }
    }
    
    realloc(bytes, length);
    return [NSData dataWithBytesNoCopy:bytes length:length];
}

@interface UIImage (custom)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@implementation UIImage (custom)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end