//
//  NMGTransfer.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 12. 14..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NMGTransferMessage;

@interface NMGTransfer : NSObject

+ (void)sendTransferMessage:(NMGTransferMessage *)transferMessage;

@end
