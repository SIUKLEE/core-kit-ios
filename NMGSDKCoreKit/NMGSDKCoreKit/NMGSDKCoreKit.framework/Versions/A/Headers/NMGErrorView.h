//
//  NMPromotionErrorView.h
//  NMPopup
//
//  Copyright (c) 2015년 Netmarble Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMGErrorView : UIView
{
    UIImageView *errorImageView;
    UILabel *errorLabel;
    
    CGFloat posX;
    CGFloat posY;
    CGSize size;
}

- (void)deviceOrientationDidChange:(CGRect)frame;

@end
