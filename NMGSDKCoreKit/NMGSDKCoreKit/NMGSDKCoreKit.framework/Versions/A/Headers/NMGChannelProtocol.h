//
//  NMGChannelProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 4. 20..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGSessionStateProtocol.h>

@class NMGResult;

@protocol NMGChannelProtocol <NSObject, NMGSessionStateProtocol>

+ (id)create;

- (void)signInWithCompletionHandler:(void(^)(NMGResult *result))completionHandler;
- (void)autoSignInWithChannelID:(NSString *)channelID completionHandler:(void(^)(NMGResult *result))completionHandler;
- (void)signOutChannelWithCompletionHandler:(void(^)(NMGResult *result))completionHandler;
- (void)signOutWithCompletionHandler:(void(^)(NMGResult *result))completionHandler;
- (void)setChannelWithPlayerID:(NSString *)playerID
                targetPlayerID:(NSString *)targetPlayerID
             completionHandler:(void(^)(NMGResult *result))completionHandler;

- (void)handleDidBecomeActive;
- (BOOL)handleOpenURL:(NSURL *)url;
- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options;

- (BOOL)checkConfiguration;
- (BOOL)provideCancelOption;
- (BOOL)provideOption;

- (NSString *)channelID;

- (NSString *)name;
- (int)code;
- (NSString *)IDKey;

- (NSString *)savedIDKey;
- (NSString *)savedTokenKey;

- (void)print;

@end
