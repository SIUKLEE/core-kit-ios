//
//  NMGAlertController.h
//  WebView
//
//  Created by LEE SIUK on 2015. 9. 23..
//  Copyright © 2015년 LEE SIUK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMGAlertController : NSObject

+ (instancetype)alertControllerWithTitle:(NSString *)title
                                 message:(NSString *)message
                       cancelButtonTitle:(NSString *)cancelButtonTitle
                        otherButtonTitle:(NSString *)otherButtonTitle;

- (void)cancelHandler:(void(^)(NMGAlertController *alertController))handler;
- (void)otherHandler:(void(^)(NMGAlertController *alertController))handler;

- (void)show;
- (void)dismiss;

@end
