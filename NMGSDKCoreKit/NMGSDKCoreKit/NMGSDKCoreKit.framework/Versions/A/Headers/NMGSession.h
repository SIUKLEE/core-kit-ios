//
//  NMGSession.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 21..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGCipher.h>

@class NMGResult;
@class NMGChannelConnectOption;
@class NMGOTPInfo;
@class NMGRestrictOTPInput;

/**
 * @brief This NMGSession class deals with connection with NetmarbleS service and related information.<br>
 * To create NMGSession instance, call createSessionWithLaunchOptions: API function. After that, use
 * sharedSession API function to get NMGSession instance, and use the NMGSession method to use session functions.<br>
 */
@interface NMGSession : NSObject {
}

/**
 * @brief Creates NMGSession instance. This method needs to be called first to use NetamrbleS SDK. If you call this method after NMGSession
 * instance is created, it returns NO.<br>
 * You can use sharedSession API function to get the created NMGSession instance.<br>
 * After creating session, player ID can be used.
 *
 * @return If success to create NMGSession instance returns true, else returns NO.
 */
+ (BOOL)createSessionWithLaunchOptions:(NSDictionary *)launchOptions;

/**
 * @brief Gets NMGSession instance.<br>
 * If the NMGSession instance is not created by using createSessionWithLaunchOptions: API function,
 * this method returns null.
 *
 * @return NMGSession instance.<br>If the NMGSession instance is not created, it returns null.
 * @see NMGSession
 */
+ (NMGSession *)sharedSession;

/**
 * @brief Sign In to NetmarbleS service.<br>
 * After signing in, game token can be used to authenticate the player in game server.
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
- (void)signInWithCompletionHandler:(void(^)(NMGResult *result))completionHandler;

/**
 * @brief Gets signed a channel.<br>
 * If channels is already connected, it returns auto signed channel.
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * -  channel Channel.
 * @see NMGResult
 */
- (void)setChannelSignInWithCompletionHandler:(void(^)(NMGResult *result, int channel))completionHandler;

/**
 * @brief Connect to channel.<br>
 * After signing in, player who has player ID connect with channel ID.<br>
 * If channel id is already connected other player ID or current channel ID is not connected with current player ID, SELECT_CHANNEL_CONNECT_OPTION result and option list that you can choose will be delivered.
 * You can use selectChannelConnectOption API to select option in option list.
 *
 * @param channel Channel.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
- (void)connectToChannel:(int)channel completionHandler:(void(^)(NMGResult *result, NSArray *channelConnectOption))completionHandler;

/**
 * @brief Select channel connect option.<br>
 * After signing in, player who has player ID connect with channel ID.
 * if you get option list, use this API function to select option.
 *
 * @param option NMGChannelConnectOption.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 * @see NMGChannelConnectOption
 */
- (void)selectChannelConnectOption:(NMGChannelConnectOption *)option completionHandler:(void(^)(NMGResult *result))completionHandler;

/**
 * @brief Disconnect from channel.
 *
 * @param channel Channel.
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
- (void)disconnectFromChannel:(int)channel completionHandler:(void(^)(NMGResult *result))completionHandler;

/**
 * @brief Issue a (OTP)One Time Password<br>
 * After signing in, it will be issued to OTP.<br>
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 * @see NMGOTPAuthenticationHistory
 */
- (void)issueOTPWithCompletionHandler:(void(^)(NMGResult *result, NSString *OTP, NSArray *OTPAuthenticationHistoryArray))completionHandler;

/**
 * @brief Check the OTP is correct.<br>
 * If OTP is correct, you can get a playerID and region that want to move.<br>
 *
 * @param OTP one time password
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 * @see NMGOTPInfo
 */
- (void)requestOTPInfoWithOTP:(NSString *)OTP completionHandler:(void(^)(NMGResult *result, NMGOTPInfo *OTPInfo, NMGRestrictOTPInput *restrictOTPInput))completionHandler;

/**
 * @brief Copy a playerID<br>
 * If OTP is correct, you can move a playerID and region.<br>
 *
 * @param completionHandler Callback function.
 *
 * The parameters of this callback are as follows.
 * - result Result.
 * @see NMGResult
 */
- (void)copyPlayerIDWithOTP:(NSString *)OTP completionHandler:(void(^)(NMGResult *result, NMGRestrictOTPInput *restrictOTPInput))completionHandler;

/**
 * @brief Set a region.
 *
 * @param region Region.
 *
 * @deprecated
 */
- (void)setRegion:(NSString *)region __attribute__((deprecated));

/**
 * @brief Gets a region.<br>
 * This method always returns region.<br>
 *
 * @return Region.
 */
- (NSString *)region;

/**
 * @brief Gets a player ID.<br>
 * This method always returns player ID.<br>
 *
 * @return Player ID.
 */
- (NSString *)playerID;

/**
 * @brief Gets a game token.<br>
 * This method returns a game token when signed in successfully.
 *
 * @return Game token.
 */
- (NSString *)gameToken;

/**
 * @brief Gets a cipher data.<br>
 * This method returns a cipher data when signed in successfully.
 *
 * @return Cipher data.
 * @see NMGCipher
 */
- (NMGCipher *)cipherData:(NMGCipherType)cipherType;

/**
 * @brief NetmarbleS 인증 서버에 저장된 채널 정보<br>
 * 로그인된 PlayerID에 연결된 채널 리스트를 JSON format의 String을 조회 할 수 있습니다.
 *
 * @return 연결된 채널 리스트
 */
- (NSString *)connectedChannelsByAuthServer;

/**
 * @brief Gets a channel ID.<br>
 * This method returns a channel ID when this player connected to this channel.
 *
 * @param channel Channel.
 * @return Channel ID.
 */
- (NSString *)channelIDWithChannel:(int)channel;

/**
 * @since v3.8.0
 * @brief Gets a joined country code<br>
 * This method returns a joined country code when signed in successfully.
 *
 * @return IP address.
 */
- (NSString *)joinedCountryCode;

/**
 * @brief Gets a country code.<br>
 * This method returns a country code(get by IP address) when signed in successfully.
 *
 * @return Country code.
 */
- (NSString *)countryCode;

/**
 * @since v3.8.0
 * @brief Gets a IP address.<br>
 * This method returns a IP address when signed in successfully.
 *
 * @return IP address.
 */
- (NSString *)IPAddress;

/**
 * @since v3.9.0
 * @brief 현재 WorldID를 저장합니다.
 * @param worldID worldID.
 */
- (void)setWorld:(NSString *)worldID;

/**
 * @since v3.9.0
 * @brief 현재 WorldID를 가져옵니다.
 * @return worldID.
 */
- (NSString *)getWorld;

/**
 * @since v3.9.0
 * @brief 현재 WorldID를 삭제합니다
 */
- (void)removeWorld;

/**
 * @brief Resets session.<br>
 * Player ID will be changed.
 */
- (void)resetSession;

@end
