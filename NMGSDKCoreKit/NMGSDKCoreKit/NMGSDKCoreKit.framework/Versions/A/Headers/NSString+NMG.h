//
//  Define.h
//  NSP2HttpConnector
//
//  Created by SIUK LEE on 12. 9. 17..
//  Copyright (c) 2012년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(NMG)

- (BOOL) isContained:(NSString*)str;
- (NSString *)stringByURLDecoding;
- (NSString*) stringByURLEncoding;
- (NSString*) stringByURLEncodingEuc_kr;
- (NSString*) stringByStrippingHTML;
- (NSString*) substringFromToken:(NSString*)token;
- (NSDictionary*) parsingByJson;
- (id)jsonObject;
- (NSMutableDictionary *) dictionaryFromQueryComponents;
- (BOOL)isEqualToIgnoreCaseString:(NSString*)str;

@end
