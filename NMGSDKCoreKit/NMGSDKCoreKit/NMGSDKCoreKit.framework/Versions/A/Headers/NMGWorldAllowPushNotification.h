//
//  NMGWorldAllowPushNotification.h
//  NetmarbleS
//
//  Created by Jinhee on 2016. 1. 25..
//  Copyright © 2016년 Netmarble. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, NMGAllowPushNotification)
{
    NMGAllowPushNotificationNone = 0,
    NMGAllowPushNotificationOn,
    NMGAllowPushNotificationOff,
};

@interface NMGWorldAllowPushNotification : NSObject
{
    NSString *_worldID;
    NMGAllowPushNotification _notice;
    NMGAllowPushNotification _game;
    NMGAllowPushNotification _nightNotice;
}

- (instancetype)initWithWorldID:(NSString *)worldID
                         notice:(NMGAllowPushNotification) notice
                           game:(NMGAllowPushNotification) game
                    nightNotice:(NMGAllowPushNotification) nightNotice;

@property (nonatomic, retain, readonly) NSString *worldID;

@property (nonatomic, assign) NMGAllowPushNotification notice;

@property (nonatomic, assign) NMGAllowPushNotification game;

@property (nonatomic, assign) NMGAllowPushNotification nightNotice;

@end