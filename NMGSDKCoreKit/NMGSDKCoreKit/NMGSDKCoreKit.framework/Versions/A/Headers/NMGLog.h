//
//  NMGLog.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 22..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGConfiguration.h>

#define NM_LOG(format,...) [NMGLog logWithDate:[NSDate date] fileName:__FILE__ functionName:__func__ lineNumber:__LINE__ isHttpLog:NO input:(format), ##__VA_ARGS__]

#define NM_HTTP_LOG(format,...) [NMGLog logWithDate:[NSDate date] fileName:__FILE__ functionName:__func__ lineNumber:__LINE__ isHttpLog:YES input:(format), ##__VA_ARGS__]

/**
 * @brief This NMGLog class provides log API functions.
 */
@interface NMGLog : NSObject

/**
 * @brief Send a game log to log server.
 *
 * @param logID Log ID.
 * @param detailID Detail ID.
 * @param pcSeq PCSeq.
 * @param logDataDic Log data dictionary.
 */

+ (void)sendGameLogWithLogID:(int)logID
                    detailID:(int)detailID
                       pcSeq:(NSString *)pcSeq
                     logData:(NSDictionary *)logDataDic;

+ (void)logWithDate:(NSDate *)date
           fileName:(const char *)fileName
       functionName:(const char *)functionName
         lineNumber:(int)lineNumber
          isHttpLog:(BOOL)isHttpLog
              input:(NSString *)input, ...;

@end


