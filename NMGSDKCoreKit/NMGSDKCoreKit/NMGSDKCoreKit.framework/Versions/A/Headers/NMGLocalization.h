//
//  NMGLocalization.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 8. 6..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NMLocalizedString(key, comment) \
[[NMGLocalization sharedInstance] localizedStringForKey:(key) value:(comment)]

@interface NMGLocalization : NSObject
{
    NSSet *languagePackage;
}

+ (NMGLocalization *)sharedInstance;

- (void)setLanguage;
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

@end
