//
//  Define.h
//  NSP2HttpConnector
//
//  Created by SIUK LEE on 12. 9. 17..
//  Copyright (c) 2012년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(NMG)

- (NSData *) getJsonData;
- (NSString *) getJsonString;
- (id) objectOrNilForKey:(NSString*)key;

@end
