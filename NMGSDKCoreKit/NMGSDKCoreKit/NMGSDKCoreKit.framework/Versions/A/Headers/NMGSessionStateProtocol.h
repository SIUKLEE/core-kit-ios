//
//  NMSessionState.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 4. 20..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NMG_SESSION_STATE_WORLD_CHANGED                 1
#define NMG_SESSION_STATE_WORLD_REMOVED                 2
#define NMG_SESSION_STATE_CREATE_CHANNEL_CONNECTED      3
#define NMG_SESSION_STATE_LOAD_CHANNEL_CONNECTED        4
#define NMG_SESSION_STATE_CHANNEL_CONNECTED             5
#define NMG_SESSION_STATE_LANGUAGE_CHANGED              6

@protocol NMGSessionStateProtocol <NSObject>

- (void)onCreatedSession;
- (void)onInitializedSession;
- (void)onSignedSession;
- (void)onUpdatedSession:(NSNumber *)code;

@end