//
//  NMGUiViewProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 4. 20..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMGSDKCoreKit/NMGSessionStateProtocol.h>
#import <NMGSDKCoreKit/NMGUiViewHandler.h>

@protocol NMGUiViewProtocol <NSObject, NMGSessionStateProtocol>

+ (id)create;

- (void)setLocationWithLower:(NSNumber *)lower upper:(NSNumber *)upper;
- (void)showWithLocation:(NSNumber *)location eventHandler:(void(^)(NMGUiViewState uiViewState))eventHandler;
- (void)close;

@end
