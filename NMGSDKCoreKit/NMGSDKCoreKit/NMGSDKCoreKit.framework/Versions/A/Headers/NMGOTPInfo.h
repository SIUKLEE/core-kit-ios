//
//  NMGOTPInfo.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2015. 7. 31..
//  Copyright (c) 2015년 Netmarble. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief This NMGOTPInfo class provides OTP information.
 */
@interface NMGOTPInfo : NSObject
{
    NSString *_OTP;
    NSString *_playerID;
    NSString *_region;
    NSArray *_OTPAuthenticationHistoryArray;
}

- (instancetype)initWithOTP:(NSString *)OTP
                   PlayerID:(NSString *)playerID
                     region:(NSString *)region
OTPAuthenticationHistoryArray:(NSArray *)OTPAuthenticationHistoryArray;

/**
 * @brief OTP (read only)
 */
@property (nonatomic, retain, readonly) NSString *OTP;

/**
 * @brief player ID.(read only)
 */
@property (nonatomic, retain, readonly) NSString *playerID;

/**
 * @brief region.(read only)
 */
@property (nonatomic, retain, readonly) NSString *region;

/**
 * @brief OTP authentication history.(read only)
 */
@property (nonatomic, retain, readonly) NSArray *OTPAuthenticationHistoryArray;

@end
