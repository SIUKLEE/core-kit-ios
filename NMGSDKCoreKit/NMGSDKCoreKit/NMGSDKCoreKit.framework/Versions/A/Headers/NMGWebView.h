//
//  NMGWebView.h
//  WebView
//
//  Created by LEE SIUK on 2015. 9. 18..
//  Copyright © 2015년 LEE SIUK. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NMGWebViewDelegate;

//#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0)
//
//#import <WebKit/WebKit.h>
//
//@interface NMGWebView : WKWebView <WKUIDelegate, WKNavigationDelegate>
//
//#else

@interface NMGWebView : UIWebView <UIWebViewDelegate>

//#endif

@property (nonatomic, retain) id <NMGWebViewDelegate> webViewDelegate;

- (void)loadURL:(NSString *)URL;

- (void)stringByEvaluateJavaScript:(NSString *)javaScriptString completionHandler:(void(^)(NSString *value))completionHandler;

- (NSString *)currentURL;

@end

@protocol NMGWebViewDelegate <NSObject>

@optional
- (void)didStartLoad:(NMGWebView *)webView;
- (void)didFinishLoad:(NMGWebView *)webView;

- (void)didFailLoad:(NMGWebView *)webView error:(NSError *)error;

- (BOOL)shouldStartLoadWithURL:(NSString *)URL webView:(NMGWebView *)webView;
@end


