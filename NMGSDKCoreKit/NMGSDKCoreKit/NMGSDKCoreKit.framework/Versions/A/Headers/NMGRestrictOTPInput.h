//
//  NMGRestrictOTPInput.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2015. 8. 13..
//  Copyright (c) 2015년 Netmarble. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief This NMGRestrictOTPInput class provides fail count and retry date time
 */
@interface NMGRestrictOTPInput : NSObject
{
    int _failCount;
    NSString *_retryDatetime;
}

- (instancetype)initWithFailCount:(int)failCount
                    retryDatetime:(NSString *)retryDatetime;

/**
 * @brief Fail Count (read only)
 */
@property (nonatomic, assign, readonly) int failCount;

/**
 * @brief Retry Date Time (read only)
 */
@property (nonatomic, retain, readonly) NSString *retryDatetime;

@end
