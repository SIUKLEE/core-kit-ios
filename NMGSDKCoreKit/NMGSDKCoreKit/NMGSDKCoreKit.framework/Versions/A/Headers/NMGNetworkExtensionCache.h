//
//  NMGNetworkExtensionCache.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 9. 19..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMGNetworkExtensionCache : NSObject
{
    NSMutableDictionary *_networkDic;
    
    NSMutableArray *_removeHandler;
    NSMutableArray *_addHandler;
    NSMutableArray *_updatedHandler;
}

+ (void)create;
+ (NMGNetworkExtensionCache *)sharedInstance;

- (void)setObject:(NSString *)object forKey:(NSString *)key;
- (NSString *)objectForKey:(NSString *)key;
- (void)removeObjectForKey:(NSString *)key;

- (NSDictionary *)networkExtensionCache;

- (void)elementRemovedWithCacheEventHandler:(void(^)(NSDictionary *cache, NSString *key))cacheEventHandler;
- (void)elementAddWithCacheEventHandler:(void(^)(NSDictionary *cache, NSString *key))cacheEventHandler;
- (void)elementUpdatedWithCacheEventHandler:(void(^)(NSDictionary *cache, NSString *key))cacheEventHandler;

@end
