//
//  NMGUiViewHandler.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 5. 5..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

/**
 * @brief Enum for the view state.
 */
typedef enum
{
    /**
     * Opened.
     */
    NMGUiViewStateOpened = 0,
    /**
     * Closed.
     */
    NMGUiViewStateClosed,
    /**
     * Failed.
     */
    NMGUiViewStateFailed,
    /**
     * Rewarded.
     */
    NMGUiViewStateRewarded
} NMGUiViewState;