//
//  NMGCipher.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2014. 7. 28..
//  Copyright (c) 2014년 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief Enum for cipher type.
 */
typedef enum
{
    /**
     * RC4_40.
     */
    NMGCipherType_RC4_40 = 0x00,
    /**
     * AES_128_CBC.
     */
    NMGCipherType_AES_128_CBC,
} NMGCipherType;

/**
 * @brief This NMGResult class is used to manage the cipher data.<br>
 * If need to encrypt the packet data between game client and game server, this cipher data can be used to encrypt.
 */
@interface NMGCipher : NSObject
{
    NMGCipherType _cipherType;
    NSString *_secretKey;
    NSString *_aesInitialVector;
}

- (id)initWithCipherType:(NMGCipherType)cipherType secretKey:(NSString *)secretKey AESInitialVector:(NSString *)vector;

/**
 * @brief Gets the cipher type.
 *
 * @return Cipher type.
 * @see NMGCipherType
 */
@property (nonatomic, assign, readonly) NMGCipherType cipherType;

/**
 * @brief Gets the secret key.
 *
 * @return Secret key.
 */
@property (nonatomic, retain, readonly) NSString *secretKey;

/**
 * @brief Gets the AES IV(Initial vector).
 *
 * @return AES IV(Initial vector).
 */
@property (nonatomic, retain, readonly) NSString *aesInitialVector;

/**
 * @brief Encrypts the data string.
 *
 * @param plainText Raw data string.
 * @return Encrypted string.
 */
- (NSString *)encryptWithPlainText:(NSString *)plainText;

/**
 * @brief Decrypts the data string.
 *
 * @param cipherText Encrypted data string.
 * @return Decrypted string.
 */
- (NSString *)decryptWithCipherText:(NSString *)cipherText;

@end
