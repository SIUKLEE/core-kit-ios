//
//  NMGDeepLinkProtocol.h
//  NMGSDKCoreKit
//
//  Created by SIUK LEE on 2016. 5. 3..
//  Copyright © 2016년 SIUK LEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NMGDeepLinkProtocol <NSObject>

+ (id)create;

- (void)onDeepLink:(NSURL *)deepLinkURL;

@end
