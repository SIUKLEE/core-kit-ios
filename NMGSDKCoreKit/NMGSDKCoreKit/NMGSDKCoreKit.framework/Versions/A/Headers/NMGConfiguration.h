//
//  NMGConfiguration.h
//  NMCommon
//
//  Copyright (c) 2014 Netmarble Games. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CONFIGURATION_FILENAME @"NMConfiguration"

#define NMGSDK_CORE_VERSION_STRING @"4.3.0.11"

typedef enum
{
    /**
     * None.
     */
    NMGLanguageNone = 0,
    /**
     * Korean.
     */
    NMGLanguageKorean,
    /**
     * Japanese.
     */
    NMGLanguageJapanese,
    /**
     * English.
     */
    NMGLanguageEnglish,
    /**
     * Thai.
     */
    NMGLanguageThai,
    /**
     * Simplified Chinese.
     */
    NMGLanguageSimplifiedChinese,
    /**
     * Traditional Chinese.
     */
    NMGLanguageTraditionalChinese,
    /**
     * Turkish
     */
    NMGLanguageTurkish,
    /**
     * Arabic
     */
    NMGLanguageArabic,
    /**
     * French
     */
    NMGLanguageFrench,
    /**
     * Italian
     */
    NMGLanguageItalian,
    /**
     * German
     */
    NMGLanguageGerman,
    /**
     * Spanish
     */
    NMGLanguageSpanish,
    /**
     * Portuguese
     */
    NMGLanguagePortuguese,
    /**
     * Indonesian
     */
    NMGLanguageIndonesian,
    /**
     * Russian
     */
    NMGLanguageRussian,
    /** @since 2017.11.30 v4.3.0
     * Dutch
     */
    NMGLanguageDutch,
    /** @since 2017.11.30 v4.3.0
     * Vietnamese
     */
    NMGLanguageVietnamese,
} NMGLanguage;

/**
 * @brief This NMGConfiguration Deals with the configuration of NetmarbleS SDK.
 */
@interface NMGConfiguration : NSObject

/**
 * @brief Gets NetmarbleS SDK version.
 * @return NetmarbleS SDK version.
 */
+ (NSString *)getSDKVersion;

/**
 * @brief Returns the URL of GMC2 service.
 * @return The URL of GMC2 service.
 */
+ (NSString *)getGMC2URL;

/**
 * @brief Sets the URL of GMC2 service.
 * @param GMC2URL The URL of GMC2 service.
 */
+ (void)setGMC2URL:(NSString *)GMC2URL;

/**
 * @brief Gets the game code.
 * @return Game code.
 */
+ (NSString *)getGameCode;

/**
 * @brief Sets the game code.
 * @param gameCode Game code.
 */
+ (void)setGameCode:(NSString *)gameCode;

/**
 * @brief Gets the zone.
 * @return Zone. ex)alpha, dev, real
 */
+ (NSString *)getZone;

/**
 * @brief Sets the zone.
 * @param zone Zone. ex)alpha, dev, real
 */
+ (void)setZone:(NSString *)zone;

/**
 * @brief Gets the status of log setting.
 * @return Use of log.
 */
+ (BOOL)getUseLog;

/**
 * @brief Sets the use of log.
 * @param use Use.
 */
+ (void)setUseLog:(BOOL)use;

/**
 * @brief Gets whether this game is global game.
 * @return Returns Yes if this game is global game, otherwise returns No.
 * @deprecated
 */
+ (BOOL)isGlobalGame __attribute__((deprecated));

/**
 * @brief Sets whether this game is global game.
 * @param isGlobal Sets Yes if this game is global game, otherwise sets No.
 * @deprecated
 */
+ (void)setGlobalGame:(BOOL)isGlobal __attribute__((deprecated));

/**
 * @brief Gets the timeout interval for HTTP requests.
 * @return Timeout interval second. (unit: second).
 */
+ (int)getHttpTimeOutSec;

/**
 * @brief Sets the timeout interval for HTTP requests.
 * @param sec Timeout interval second. (unit: second).
 */
+ (void)setHttpTimeOutSec:(int)sec;

/**
 * @brief Gets the max size of game data queue.
 * @return The max size of game data queue.
 */
+ (int)getMaxGameLogDataCount __attribute__((deprecated));

/**
 * @brief Sets the max size of game data queue.
 * @param count The max size of game data queue.
 */
+ (void)setMaxGameLogDataCount:(int)count __attribute__((deprecated));

/**
 * @brief Gets the time interval for sending game log datas stored.
 * @return Time interval. (unit: minute).
 */
+ (int)getSendGameLogDataIntervalMin __attribute__((deprecated));

/**
 * @brief Gets the max size of game data queue.
 * @param min Time interval. (unit: minute).
 */
+ (void)setSendGameLogDataIntervalMin:(int)min __attribute__((deprecated));

/**
 * @brief Gets the value whether to Facebook login popup webView method.
 * @return The value(enable/disable) of Facebook login popup webview method.
 */
+ (BOOL)getUseFacebookLoginViewInApp;

/**
 * @brief Sets the value whether Facebook login inapp webView use.
 * @param use The value(enable/disable) of Facebook login popup webView method.
 */
+ (void)setUseFacebookLoginViewInApp:(BOOL)use;

/**
 * @brief Gets the status of region setting.
 * @return Region mode. ex)none, automatically, manually
 * @deprecated
 */
+ (NSString *)getRegionMode __attribute__((deprecated));

/**
 * @brief Sets the region mode.
 * @param regionMode RegionMode. ex)none, automatically, manually
 * @deprecated
 */
+ (void)setRegionMode:(NSString *)regionMode __attribute__((deprecated));

/**
 * @brief Gets a fixed playerID
 * @return Use a fixed playerID
 */
+ (BOOL)getUseFixedPlayerID;

/**
 * @brief Sets a fixed playerID
 * If you want to a fixed playerID, this use.
 * @param useFixedPlayerID UseFixedPlayerID.
 */
+ (void)setUseFixedPlayerID:(BOOL)useFixedPlayerID;

/**
 * @brief Gets the SDK language. ex)@"ko", @"en", @"ja", @"th", @"zh-Hans", @"zh-Hant", @"tr", @"ar"
 * if return value is nil, SDK use the langauge set in device.
 * @return SDK language.
 * @see NMLanguage 
 */
+ (NMGLanguage)getLanguage;

/**
 * @brief Sets the SDK langauge.<br>
 * if sets nil, SDK use the langauage set in device.
 * @param language Language.
 * @see NMLanguage
 */
+ (void)setLanguage:(NMGLanguage)language;

/**
 * @brief Gets the custom language.
 * if return value is nil, SDK use the langauge set in device.
 * @return custom language.
 */
+ (NSString *)getCustomLanguage;

/**
 * @brief Sets the SDK custom langauge.<br>ex)@"fr", @"de"
 * if you want to something else language, SDK use the custom langauage set in device.
 * @param language Language.
 */
+ (void)setCustomLanguage:(NSString *)language;

/**
 * @brief Sets the OTP length
 * @param OTPLength The length of OTP.
 */
+ (void)setOTPLength:(int)OTPLength;

/**
 * @brief Gets the OTP length
 * @return OTPLength
 */
+ (int)getOTPLength;

/**
 * @brief Sets the OTP life cycle.
 * @param OTPLifeCycle The life cycle of OTP.
 */
+ (void)setOTPLifeCycle:(int)OTPLifeCycle;

/**
 * @brief Gets the OTP life cycle.
 * @return OTPLifeCycle
 */
+ (int)getOTPLifeCycle;

/**
 * @brief Sets the OTP history period
 * @param OTPHistoryPeriod The history period of OTP.
 */
+ (void)setOTPHistoryPeriod:(int)OTPHistoryPeriod;

/**
 * @brief Gets the OTP history period
 * @return OTPHistoryPeriod
 */
+ (int)getOTPHistoryPeriod;

/**
 * @since v3.8.0
 * @brief Sets the channel connect modified.
 * @param use The value(enable/disable) of channel connect modified.
 */
+ (void)setUseChannelConnectionModified:(BOOL)use;

/**
 * @since v3.9.2
 * @brief 다국어 지원 처리 단계 설정
 * @param localizedLevel 디폴트 value는 1 (번역만 지원 : 1, 번역 and UI 지원 : 2)
 */
+ (void)setLocalizedLevel:(NSString *)localizedLevel;

/**
 * @since v3.9.2
 * @brief 다국어 지원 처리 단계 설정값 조회
 */
+ (NSString *)getLocalizedLevel;

/**
 * @since v3.9.2
 * @brief 공지사항의 TitleBar를 숨김.
 * @param isHidden 디폴트 NO, Rolling형 YES.
 */
+ (void)setIsHiddenNoticeTitleBar:(BOOL)isHidden;

/**
 * @since v3.9.2
 * @brief 공지사항 TitleBar 숨김 여부 조회.
 */
+ (BOOL)getIsHiddenNoticeTitleBar;

/**
 * @since v3.11.0
 * @brief 애플 검수 설정 조회
 */
+ (BOOL)isInReview;

/**
 * @since v3.11.0
 * @brief 애플 검수 설정
 */
+ (void)setIsInReview:(BOOL)isInReview;

@end
