//
//  NMGOTPAuthenticationHistory.h
//  NetmarbleS
//
//  Created by LeeSiUk on 2015. 7. 31..
//  Copyright (c) 2015년 Netmarble. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief This NMGOTPAuthenticationHistory class provides OTP authentication history of user.
 */
@interface NMGOTPAuthenticationHistory : NSObject
{
    NSString *_gameCode;
    NSString *_playerID;
    NSString *_creationDate;
}

- (instancetype)initWithGameCode:(NSString *)gameCode
                        playerID:(NSString *)playerID
                    creationDate:(NSString *)creationDate;

/**
 * @brief Game Code.(read only)
 */
@property (nonatomic, retain, readonly) NSString *gameCode;

/**
 * @brief Player ID.(read only)
 */
@property (nonatomic, retain, readonly) NSString *playerID;

/**
 * @brief Creation Date.(read only)
 */
@property (nonatomic, retain, readonly) NSString *creationDate;

@end
