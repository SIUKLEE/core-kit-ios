#
# Be sure to run `pod lib lint NMGSDKCoreKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NMGSDKCoreKit'
  s.version          = '4.3.0'
  s.summary          = 'NetmarbleS. NMGSDKCoreKit.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  #s.description      = ''

  s.homepage         = 'https://bitbucket.org/SIUKLEE/core-kit-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'SIUK LEE' => 'lsu8290@netmarble.com' }
  s.source           = { :git => 'https://bitbucket.org/SIUKLEE/core-kit-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.platform = :ios, '7.0'


  #s.source_files = 'NMGSDKCoreKit/AFNetworking/*.{h,m}, NMGSDKCoreKit/KeychainItemWrapper/*.{h,m}, NMGSDKCoreKit/Socket/*.{h,m}, NMGSDKCoreKit/protobuf3/*.{h,m}, NMGSDKCoreKit/protobuf3/google/protobuf/*.{h,m}'
  #s.ios.deployment_target = '7.0'
  #s.ios.vendored_frameworks = 'NMGSDKCoreKit/NMGSDKCoreKit.framework'
  #s.libraries      = 'z', 'sqlite3'
  #s.requires_arc   = true
  #s.resources = 'NMGSDKCoreKit/NMGSDKCoreKit.framework/Resources'
  
  # s.resource_bundles = {
  #   'NMGSDKCoreKit' => ['NMGSDKCoreKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

  s.subspec 'NMGSDKCoreKit' do |sc|
    sc.ios.source_files = 'NMGSDKCoreKit/AFNetworking/*.{h,m}'
    sc.ios.vendored_frameworks = 'NMGSDKCoreKit/NMGSDKCoreKit.framework'
    sc.ios.libraries      = 'z', 'sqlite3'
    #sc.ios.preserve_paths =  'NMGSDKCoreKit/NMGSDKCoreKit.framework/*'
  end
end
